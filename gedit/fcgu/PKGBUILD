# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
options=(debug !strip)

pkgname=gedit
pkgver=3.38.1
pkgrel=1.1
pkgdesc="GNOME Text Editor"
url="https://wiki.gnome.org/Apps/Gedit"
arch=(x86_64)
license=(GPL)
depends=(gtksourceview4 gsettings-desktop-schemas libpeas gspell python-gobject
         dconf tepl)
makedepends=(yelp-tools vala gobject-introspection git gtk-doc meson
             appstream-glib desktop-file-utils libxml2)
optdepends=('gedit-plugins: Additional features')
conflicts=('gedit-code-assistance<=3.16.0+4+gd19b879-1')
groups=(gnome)
_commit=3c327559e6b59a0d7b4c8c61764685224ad10752  # tags/3.38.1^0
source=("git+https://gitlab.gnome.org/GNOME/gedit.git#commit=$_commit"
        "git+https://gitlab.gnome.org/GNOME/libgd.git"
        ## cherrypick patch to fix up gedit with never nautilus
        "0001-drop-use-of-confirm-trash.diff::https://gitlab.gnome.org/GNOME/gedit/-/commit/8f6490c602991a456413e0053429bb7b4c70d4b6.diff")
sha256sums=('SKIP'
            'SKIP'
            'bcee6571ff2ef5ec58284fdfc5b9add4f1078925937043c8425ae0c621bbf634')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/-/+/g'
}

prepare() {
  cd $pkgname
  git apply -3 ../0001-drop-use-of-confirm-trash.diff
  git submodule init
  git submodule set-url subprojects/libgd "$srcdir/libgd"
  git submodule update
}

build() {
  arch-meson $pkgname build \
    -D gtk_doc=true \
    -D require_all_tests=true
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  DESTDIR="$pkgdir" meson install -C build
}
